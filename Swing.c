//#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/prctl.h>

typedef struct Country Country;
typedef struct CountryLinked CountryLinked;

struct Country {
  unsigned long long int pop;
  char Name[2];
  unsigned char weight;
};

struct CountryLinked {
  CountryLinked* Next;
  Country* Val;
};

void
push(CountryLinked** head, Country* Val)
{
  if (*head == NULL){
    *head = malloc(sizeof(CountryLinked));
    (*head) -> Val = Val;
    (*head) -> Next = NULL;
    return;
  }
  CountryLinked* current = *head;
  while (current -> Next != NULL)
    current = current -> Next;
  current -> Next = malloc(sizeof(CountryLinked));
  current -> Next -> Val = Val;
  current -> Next -> Next = NULL;
  return;
}

Country*
pop(CountryLinked** head)
{
  if (*head == NULL)
    return NULL;
  else {
    Country* Res = (*head) -> Val;
    *head = (*head) -> Next;
    return Res;
  }
}

unsigned char
Length(CountryLinked* Countries)
{
  unsigned char Res=0;
  while (Countries != NULL){
    Res++;
    Countries = Countries -> Next;
  }
  return Res;
}

CountryLinked* 
ReadCountryFile(char *file)
{
  FILE *fd;
  char *Res = NULL;
  size_t len = 0;
  ssize_t nlines;
  fd = fopen(file, "r");
  if (fd == NULL)
    return NULL;
  CountryLinked* CountryStack = NULL;
  while ((nlines = getline(&Res, &len, fd) != -1)){
    Country* tmp = malloc(sizeof(Country));
    tmp -> Name[0] = Res[0];
    tmp -> Name[1] = Res[1];
    char readpop[255];
    for (unsigned char i = 2; Res[i] != 0; i++)
      readpop[i-2] = Res[i];
    tmp -> pop = atoll(readpop);
    tmp -> weight = 1;
    push(&CountryStack, tmp);
  }
  fclose(fd);
  return CountryStack;
}

void
printCountryList(CountryLinked* head){
  if (head == NULL)
    return;
  printf("Nm: Popul, wght\n");
  while (head != NULL){
    printf("%s: %lld, %d\n", head -> Val -> Name, head -> Val -> pop, head -> Val -> weight);
    head = head -> Next;
  }
  return;
}

unsigned long long int
TotPop(CountryLinked* Countries)
{
  unsigned long long int poptot = 0;
  while (Countries != NULL){
    poptot += Countries -> Val -> pop;
    Countries = Countries -> Next;
  }
    return poptot;
}

unsigned long long
TotPopArray(Country* Countries, unsigned char length)
{
  unsigned long long poptot = 0;
  length--;
  for (; length >= 0; length--)
    poptot += Countries[length].pop;
  return poptot;
}

/*
 * Swing if:
 * 	((Countries in Coalition + my Country = more than half countries in eu) AND (Population of countries in Coalition + Pop In my Country = more than 65% of total eu pop.)) AND
 * 	((Countries in Coalition = less than half countries is eu) OR (Population of countries in Coalition = less than 65% of total eu pop.)) OR => my country is 
 * 	(Countries in Coalition == amount of countries in eu - 5) => 4 countries can't block the rest
 */
/*
char
IsSwing(CountryLinked* Countries, Country* Country, unsigned char totCountries, unsigned long long int PopTot)
{
  unsigned long long int PopInTot = 0;
  unsigned char NbCountries = 0;
  unsigned long long int CountriesForSwing = totCountries*0.55;
  unsigned long long int PopForSwing = PopTot*0.65;
  while (Countries != NULL){
    PopInTot += Countries -> Val -> pop;
    NbCountries+= Countries -> Val -> weight;
    Countries = Countries -> Next;
  }
  // Swing if Country added adds enough population or countrynumber tot over the needed amount or country added
  // 50% nb Countries / 0.65 pop, 4 countries can never stop a coalition
  return (((NbCountries+Country -> weight > CountriesForSwing && PopInTot + Country->pop > PopForSwing) && 
	(NbCountries <= CountriesForSwing || PopInTot <= CountriesForSwing)) || 
	(NbCountries + Country -> weight == totCountries - 4))
}
*/

Country*
LinkedToArray(CountryLinked* Countries, unsigned char length)
{
  Country* Res = malloc(sizeof(Country)*length);
  for (unsigned char i = 0; i < length; i++){
    Res[i].pop = Countries -> Val -> pop;
    Res[i].weight = Countries -> Val -> weight;
    Res[i].Name[0] = Countries -> Val -> Name[0];
    Res[i].Name[1] = Countries -> Val -> Name[1];
    Countries = Countries -> Next;
  }
  return Res;
}

void
FreeCoutryLinked(CountryLinked* head)
{
  while (head != NULL){
    CountryLinked* tmp = head;
    head = head -> Next;
    free(tmp);
  }
  return;
}

/*
 * check for each possible coalition if Country has a swing
 */
/*
unsigned long long int
GetNbOfSwings(Country* Countries, unsigned char CountryNb, unsigned char NbCountries, unsigned long long int TotPop)
{
  unsigned long long int Res = 0;
  unsigned long long int mask=0;//this should be 0x111...
  for (unsigned char i = 0; i < NbCountries; i++)// make mask with all countries on 1 except Country
    if (i != CountryNb)
      mask += (1 << i);
  while (mask != 0){//build a linkedlist based on bitmask NbCountries and check if swing
    CountryLinked* head = NULL;
    for (unsigned char i = 0; i < NbCountries; i++)//build linkedlist
      if ((mask & (1 << i)) && i != CountryNb)
	push(&head, &Countries[i]);
    if (IsSwing(head, &Countries[CountryNb], NbCountries, TotPop))
	Res++;
    mask--;
    if (mask & (1 << CountryNb))
      mask -= (1 << CountryNb);
    FreeCoutryLinked(head);
  }
  return Res;
}
*/

/*
 * check for each possible coalition if Country has a swing
 */
unsigned long long int
GetNbOfSwingsDirect(Country* Countries, unsigned char CountryNb, unsigned char NbCountries, unsigned long long int TotPop)
{
  unsigned long long Res = 0;
  unsigned long long mask = 0;//this should be 0x111...
  for (unsigned char i = 0; i < NbCountries; i++)// make mask with all countries on 1 except Country
    if (i != CountryNb)
      mask += (1 << i);
  unsigned long long PopForSwing = TotPop*0.65;
  unsigned long long CountriesForSwing = NbCountries*0.55;
  while (mask != 0){
    unsigned long long CntryPop = 0;
    unsigned char CountriesWght = 0;
    for (unsigned char i = 0; i < NbCountries; i++)//get totals
      if ((mask & (1 << i)) && (i != CountryNb)){
	CntryPop += Countries[i].pop;
	CountriesWght += Countries[i].weight;
      }
    if (((CountriesWght + Countries[CountryNb].weight > CountriesForSwing && 
	CntryPop + Countries[CountryNb].pop > PopForSwing) && 
	(CountriesWght <= CountriesForSwing || CntryPop <= PopForSwing)) || 
	(CountriesWght + Countries[CountryNb].weight == NbCountries - 3))
	Res++;
    mask--;
    if (mask & (1 << CountryNb))// since our mask is counting down this works and will only change 1 bit
      mask -= (1 << CountryNb);
  }
  return Res;
}

void
MkCoalition(CountryLinked* head, char Name1[2], char Name2[2])
{
//find first Country
  while(!(((head -> Val -> Name[0] == Name1[0]) && (head -> Val -> Name[1] == Name1[1])) ||
      ((head -> Val -> Name[0] == Name2[0]) && (head -> Val -> Name[1] == Name2[1])))){
    head = head -> Next;
  }
  Country* First = head -> Val;
//find second Country
  while (!((head -> Next -> Val -> Name[0] == Name1[0] && head -> Next -> Val -> Name[1] == Name1[1]) ||
    (head -> Next -> Val -> Name[0] == Name2[0] && head -> Next -> Val -> Name[1] == Name2[1]))){
    head = head -> Next;
  }
  CountryLinked* tmp = head -> Next;
  First -> pop += tmp -> Val -> pop;
  First -> weight += tmp -> Val -> weight;
  First -> Name[0] = Name1[0];
  First -> Name[1] = Name1[1];
  head -> Next = tmp -> Next;
  free(tmp);
  return;
}

int
main(int argc, char* argv[])
{
  long NbCores = sysconf(_SC_NPROCESSORS_CONF);
  CountryLinked* CountryList = ReadCountryFile(argv[1]);
  for (unsigned char i = 2;  i < argc; i+=2)
    MkCoalition(CountryList, argv[i], argv[i+1]);
  printCountryList(CountryList);
  printf("Results:\n");
  unsigned char CountryAmount = Length(CountryList);
  Country* CountryArray = LinkedToArray(CountryList, CountryAmount);
  unsigned long long int Pop = TotPop(CountryList);
  //printf("totpop: %llu, %llu\n", Pop, TotPopArray(CountryArray, CountryAmount));
  FreeCoutryLinked(CountryList);
  for (unsigned char j = 0; j < NbCores; j++){
    if (fork() == 0){
      prctl(PR_SET_NAME,"SwingChild",NULL,NULL,NULL);// setProcName
      for (unsigned char i = (CountryAmount*j)/NbCores; i < CountryAmount*(j+1)/NbCores; i++){
	//printf("%c%c: %llu\n", CountryArray[i].Name[0], CountryArray[i].Name[1], GetNbOfSwings(CountryArray, i, CountryAmount, Pop));
	printf("%c%c: %llu\n", CountryArray[i].Name[0], CountryArray[i].Name[1], GetNbOfSwingsDirect(CountryArray, i, CountryAmount, Pop));
      }
      break;
    }
  }
  int wstatus;
  for (unsigned char i = 0; i < NbCores; i++)
	  waitpid(-1, &wstatus, 0);
  return 0;
}
